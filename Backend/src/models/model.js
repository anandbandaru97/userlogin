const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  // Add a new field for the user token
  token: {
    type: String,
    unique: true,
  },
  // Other user fields...
});

const User = mongoose.model("User", userSchema);

module.exports = User;
