const sqlite3 = require("sqlite3");
const bcrypt = require("bcrypt");

const db = new sqlite3.Database("users.db");

// Create the users table if it doesn't exist
db.serialize(() => {
  db.run(`
    CREATE TABLE IF NOT EXISTS users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      username TEXT UNIQUE NOT NULL,
      email TEXT NOT NULL,
      password TEXT NOT NULL,
      age INTEGER,
      dob DATE,
      qualification TEXT,
      office TEXT,
      role TEXT,
      mobile NUMERIC
    )
  `);
});

// Function to create a new user
async function createUser(username, email, password) {
  const hashedPassword = await bcrypt.hash(password, 10);
  return new Promise((resolve, reject) => {
    db.run(
      "INSERT INTO users (username, email, password) VALUES (?, ?, ?)",
      [username, email, hashedPassword],
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve({ id: this.lastID });
        }
      }
    );
  });
}

// Function to find a user by email
function findUserByEmail(email) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE email = ?", [email], (err, row) => {
      if (err) {
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

function findUserByUsername(username) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE username = ?", [username], (err, row) => {
      if (err) {
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

function getAllUserDetails() {
  return new Promise((resolve, reject) => {
    const query = "SELECT * FROM users";
    db.all(query, [], (err, rows) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}
// Function to update a user
async function updateUser(username, updatedUserData) {
  const { email, age, dob, qualification, office, role, mobile } =
    updatedUserData;

  return new Promise((resolve, reject) => {
    db.run(
      `
      UPDATE users
      SET  age =? , dob =?,qualification = ?, office = ?, role = ?, mobile = ?
      WHERE username = ?
    `,
      [age, dob, qualification, office, role, mobile, username],
      function (err) {
        if (err) {
          reject(err);
        } else {
          // Return the updated user data
          resolve(updatedUserData);
        }
      }
    );
  });
}

async function updatePassword(email, newPassword) {
  const hashedPassword = await bcrypt.hash(newPassword, 10);

  return new Promise((resolve, reject) => {
    db.run(
      `
      UPDATE users
      SET password = ?
      WHERE email = ?
    `,
      [hashedPassword, email],
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve("Password updated successfully");
        }
      }
    );
  });
}

module.exports = {
  createUser,
  findUserByEmail,
  findUserByUsername,
  getAllUserDetails,
  updateUser,
  updatePassword,
};
