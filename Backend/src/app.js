const express = require("express");
const cookieParser = require("cookie-parser")
const cors = require("cors");
require("dotenv").config();
const userRoutes = require("../src/routes/routes");

const app = express();
const { PORT } = process.env;

app.use(express.json());
app.use(cookieParser())
app.use(cors({ origin: "*" }));

// Mount the authentication routes
app.use("/api/user", userRoutes);

app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});
