const express = require("express");
const {
  createUser,
  findUserByEmail,
  findUserByUsername,
  getAllUserDetails,
  updateUser,
  updatePassword,
} = require("../models/user");
const jwt = require("jsonwebtoken");
const {
  authenticateUser,
  authorizeUser,
} = require("../controllers/controllers");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const crypto = require("crypto");

const router = express.Router();
const jwtSecret = crypto.randomBytes(32).toString("hex");

// validate signup
const validateSignup = [
  check("username").notEmpty().isAlpha().trim(),
  check("email").isEmail().trim(),
  check("password").isLength({ min: 8 }).trim(),
];

// Signup route
router.post("/signup", validateSignup, async (req, res) => {
  // Check for validation errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const { username, email, password } = req.body;

    const existingEmail = await findUserByEmail(email);
    const existingUsername = await findUserByUsername(username);

    if (existingEmail && existingUsername) {
      return res
        .status(400)
        .json({ error: "Email and Username are already taken!" });
    } else if (existingUsername) {
      return res.status(400).json({ error: "Username is already taken!" });
    } else if (existingEmail) {
      return res.status(400).json({ error: "Email is already taken!" });
    }

    // Hash the user's password before storing it
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await createUser(username, email, hashedPassword);
    const token = jwt.sign({ username: user.username }, jwtSecret);

    res.status(201).json({ user, token });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Data validation middleware for user login
const validateLogin = [
  check("email").isEmail().trim(),
  check("password").isLength({ min: 8 }).trim(),
];

// Login route
router.post("/", validateLogin, async (req, res) => {
  // Check for validation errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const { email, password } = req.body;
    const user = await authenticateUser(email, password);

    if (user) {
      const token = jwt.sign({ username: user.username }, jwtSecret);
      res.json({ user, token });
    } else {
      res.status(401).json({ error: "Invalid email or password credentials!" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
});

function authenticateJWT(req, res, next) {
  const token = req.header("Authorization");

  if (!token) {
    return res.status(401).json({ error: "Unauthorized access" });
  }

  jwt.verify(token, jwtSecret, (err, user) => {
    if (err) {
      return res.status(403).json({ error: "Token verification failed" });
    }

    req.user = user;
    next();
  });
}

// Route to get all users (excluding password)
router.post("/users/", async (req, res) => {
  try {
    const username = req.body.username;
    const users = await getAllUserDetails();
    const token = jwt.sign({ username }, jwtSecret);

    // Get all user details (excluding password)
    const filteredUsers = users.filter((user) => user.username !== username);
    if (users.length > 0) {
      const userData = filteredUsers.map(
        ({ password, ...userData }) => userData
      );
      res.json({ userData });

    } else {
      res.status(404).json({ error: "No users found" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// // Update user details
// router.put("/:username", authorizeUser, async (req, res) => {
//   const { username } = req.params;
//   const updatedUser = req.body; // Updated user data

//   try {
//     const user = await updateUser(username, updatedUser);
//     if (user) {
//       res.json(user);
//     } else {
//       res.status(404).json({ error: "User not found" });
//     }
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: "Internal Server Error" });
//   }
// });

// Define the route to update the password
router.post("/update-password", async (req, res) => {
  const { email, newPassword, confirmPassword } = req.body;

  try {
    // Find the user by email
    const user = await findUserByEmail(email);

    // Check if the user exists
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Check if the new password and confirm password match
    if (newPassword !== confirmPassword) {
      return res
        .status(400)
        .json({ message: "New password and confirm password do not match" });
    }

    // Update the password
    await updatePassword(email, newPassword);
    res.status(200).json({ message: "Password updated successfully" });
  } catch (error) {
    console.error("Error updating password:", error);
    res.status(500).json({ message: "Error updating password" });
  }
});

module.exports = router;
