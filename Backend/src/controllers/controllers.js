const { findUserByEmail } = require("../models/user");
const bcrypt = require("bcrypt");

// Function to authenticate a user
async function authenticateUser(email, password) {
  const user = await findUserByEmail(email);
  if (!user) {
    throw Error("Invalid Email!"); // User not found
  }

  const passwordMatch = await bcrypt.compare(password, user.password);
  if (passwordMatch) {
    return user;
  } else {
    throw Error("Invalid Password!"); // Incorrect password
  }
}

// Middleware to check if a user is authorized to access a route
function authorizeUser(req, res, next) {
  const username = req.params.username;
  if (username) {
    next();
  } else {
    res.status(403).json({ msg: "Unauthorized access" });
  }
}

module.exports = {
  authenticateUser,
  authorizeUser,
};
