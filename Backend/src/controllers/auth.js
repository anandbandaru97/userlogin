const { verifyToken } = require("./jwt");

function authenticateJWT(req, res, next) {
  const token = req.header("Authorization");

  if (!token) {
    return res.status(401).json({ message: "Authentication failed" });
  }

  const user = verifyToken(token);

  if (!user) {
    return res.status(401).json({ message: "Invalid token" });
  }

  req.user = user; // Attach the user to the request object
  next();
}

module.exports = authenticateJWT;


// // Authenticated request i.e user is loggedin
// exports.isAuthenticated = (req, res, next) => {
//     if (req.isAuthenticated()) {
//       return next();
//     }
//     req.flash('danger', 'To access please login first.');
//     return res.redirect('/users/login');
//   };

// // Check if user is logged in or not
// exports.notLoggedIn = (req, res, next) => {
//     if (!req.isAuthenticated()) {
//       return next();
//     }
//     return res.redirect('/users/');
//   };