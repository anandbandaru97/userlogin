import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Header from "./components/Header";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import User from "./components/User";
import ResetPassword from "./components/ResetPassword";

// Function to check if the user is logged in
const isUserLoggedIn = () => {
  const token = localStorage.getItem("token");
  if (!token) {
    return false
  }
  return true; // Return true if the token exists
};

// Custom route guard for the /user/:username route
const PrivateUserRoute = ({ element }) => {
  return isUserLoggedIn() ? element : <Navigate to="/" />;
};

const App = () => {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route exact path="/" element={<Login />} />
        <Route exact path="/signup" element={<SignUp />} />
        <Route exact path="/resetpassword" element={<ResetPassword />} />
        <Route
          path="/:username"
          element={<PrivateUserRoute element={<User />} />}
        />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
