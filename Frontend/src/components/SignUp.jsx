import React, { useState } from "react";
import {
  Card,
  Typography,
  Button,
  TextField,
  IconButton,
  Alert,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const apiUrl = "http://localhost:7000/api/user/signup";

const SignUp = () => {
  const [userData, setUserData] = useState({
    username: "",
    email: "",
    password: "",
  });
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const navigate = useNavigate();

  const createUser = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(apiUrl, userData);
      if (res.status === 201) {
        // Assuming HTTP status 201 is used for successful creation
        setSuccess("User Created Successfully!");
        setError(null);
        setUserData({ username: "", email: "", password: "" });
        navigate(`/${userData.username}`);
      } else {
        setError("User creation failed.");
        setSuccess(null);
      }
    } catch (error) {
      console.log(error.response);

      if (error.response.status === 400) {
        setError("Invalid Credentials!");
      } 
      setSuccess(null);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserData({
      ...userData,
      [name]: value,
    });
    setError("");
    setSuccess("");
  };

  const handleClickShowPassword = () => {
    setShowPassword((show) => !show);
  };

  const formStyle = {
    padding: "1rem",
  };

  return (
    <div
      style={{
        height: "80vh",
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <Card sx={formStyle}>
        {success && (
          <Alert variant="filled" severity="success">
            {success}
          </Alert>
        )}
        <Typography variant="h3">Sign Up</Typography>
        <br />
        {error && (
          <Alert variant="filled" severity="error">
            {error}
          </Alert>
        )}
        <form
          style={{
            display: "flex",
            flexDirection: "column",
            height: "20rem",
            width: "15rem",
          }}
        >
          <TextField
            label="User Name"
            name="username"
            type="text"
            value={userData.username}
            required
            onChange={handleChange}
          />
          <br />
          <TextField
            label="Email"
            name="email"
            type="email"
            value={userData.email}
            required
            onChange={handleChange}
          />
          <br />
          <TextField
            label="Password"
            name="password"
            value={userData.password}
            required
            onChange={handleChange}
            style={{ width: "100%" }}
            type={showPassword ? "text" : "password"}
            InputProps={{
              endAdornment: (
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  edge="end"
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              ),
            }}
          />
          <br />
          <Button
            onClick={createUser}
            variant="contained"
            style={{ width: "10rem" }}
          >
            SignUp
          </Button>
          <Link to="/">
            <Button>Back</Button>
          </Link>
        </form>
      </Card>
    </div>
  );
};

export default SignUp;
