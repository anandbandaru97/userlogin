import React, { useState } from "react";
import {
  Alert,
  Button,
  Popover,
  Table,
  TableBody,
  TableRow,
  TextField,
} from "@mui/material";
import axios from "axios";

const UpdateUser = ({ user }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [success, setSuccess] = useState(null);
  const [updateUser, setUpdateUser] = useState({
    age: user.age,
    dob: user.dob,
    qualification: user.qualification,
    office: user.office,
    role: user.role,
    mobile: user.mobile,
  });

  const apiUrl = `http://localhost:7000/api/user/${user.username}`;

  const handleUpdate = async () => {
    try {
      const response = await axios.put(apiUrl, updateUser);
      // alert("User Updated Successfully");
      setSuccess("User Updated Successfully!")
      console.log("User updated:", response.data);
      closePopover();
    } catch (error) {
      console.error("Error updating user:", error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUpdateUser({
      ...updateUser,
      [name]: value,
    });
  };

  const openPopover = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const closePopover = () => {
    setAnchorEl(null);
  };

  return (
    <>
      {success && (
        <Alert variant="filled" severity="success">
          {success}
        </Alert>
      )}
      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={closePopover}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <div style={{ padding: "1rem" }}>
          <Table>
            <TableBody>
              <br />
              <TableRow>
                <TextField
                  label="New Age"
                  variant="outlined"
                  name="age"
                  value={updateUser.age}
                  onChange={handleChange}
                />
              </TableRow>
              <br />
              <TableRow>
                <TextField
                  label="New DOB"
                  variant="outlined"
                  name="dob"
                  value={updateUser.dob}
                  onChange={handleChange}
                />
              </TableRow>
              <br />
              <TableRow>
                <TextField
                  label="New qualification"
                  variant="outlined"
                  name="qualification"
                  value={updateUser.qualification}
                  onChange={handleChange}
                />
              </TableRow>
              <br />
              <TableRow>
                <TextField
                  label="New Office"
                  variant="outlined"
                  name="office"
                  value={updateUser.office}
                  onChange={handleChange}
                />
              </TableRow>
              <br />
              <TableRow>
                <TextField
                  label="New Role"
                  variant="outlined"
                  name="role"
                  value={updateUser.role}
                  onChange={handleChange}
                />
              </TableRow>
              <br />
              <TableRow>
                <TextField
                  label="New Mobile"
                  variant="outlined"
                  name="mobile"
                  value={updateUser.mobile}
                  onChange={handleChange}
                />
              </TableRow>
              <br />
            </TableBody>
          </Table>
          <Button variant="contained" onClick={handleUpdate}>
            Save
          </Button>
        </div>
      </Popover>

      <Button variant="contained" onClick={openPopover}>
        Edit
      </Button>
    </>
  );
};

export default UpdateUser;
