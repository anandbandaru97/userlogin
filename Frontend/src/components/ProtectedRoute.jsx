import React from 'react';
import { Route, Link } from 'react-router-dom';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  const jwtToken = localStorage.getItem('jwtToken');

  return (
    <Route
      {...rest}
      render={(props) =>
        jwtToken ? (
          <Component {...props} />
        ) : (
          <Link to="/" />
        )
      }
    />
  );
};

export default ProtectedRoute;
