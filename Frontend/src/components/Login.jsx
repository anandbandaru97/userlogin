import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  Button,
  Card,
  TextField,
  Typography,
  IconButton,
  Alert,
} from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import axios from "axios";

const apiUrl = "http://localhost:7000/api/user";

const Login = () => {
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const navigate = useNavigate();

  const handleEmail = (email) => {
    setUserEmail(email);
    setError("");
  };

  const handlePassword = (password) => {
    setPassword(password);
    setError("");
  };

  const handleClickShowPassword = () => {
    setShowPassword((show) => !show);
  };

  const handleLogin = () => {
    if (userEmail && userPassword) {
      // Send a POST request to your login API endpoint
      axios
        .post(apiUrl, { email: userEmail, password: userPassword })
        .then((res) => {
          const { user, token } = res.data;
          // If the login is successful, navigate to the user's profile or dashboard
            localStorage.setItem("token", token);
            setSuccess("Login successful");
            setError(null);
            setUserEmail("");
            setPassword("");
            navigate(`/${user.username}`);
        })
        .catch((error) => {
          if (error.response && error.response.status === 401) {
            // The server responded with a 401 status code (Unauthorized)
            setError("Unauthorized Login!");
            setSuccess(null);
          } else {
            setError("Invalid Email or Password!");
            setSuccess(null);
          }
          console.error("Axios Error", error);
        });
    } else {
      setError("Email and Password are required");
      setSuccess(null);
    }
  };

  const formStyle = {
    padding: "1rem",
  };

  const buttonStyle = {
    marginTop: "-.5rem",
  };

  return (
    <div
      style={{
        height: "80vh",
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <Card sx={formStyle}>
        {success && (
          <Alert variant="filled" severity="success">
            {success}
          </Alert>
        )}
        <Typography variant="h3">Login</Typography>
        <br />
        {error && (
          <Alert variant="filled" severity="error">
            {error}
          </Alert>
        )}
        <form
          style={{
            display: "flex",
            flexDirection: "column",
            height: "15rem",
            width: "15rem",
          }}
        >
          <TextField
            label="Email"
            id="email"
            type="email"
            value={userEmail}
            required
            onChange={(e) => handleEmail(e.target.value)}
          ></TextField>
          <br />
          <div>
            <TextField
              label="Password"
              id="password"
              style={{ width: "100%" }}
              type={showPassword ? "text" : "password"}
              value={userPassword}
              required
              onChange={(e) => handlePassword(e.target.value)}
              InputProps={{
                endAdornment: (
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                ),
              }}
            />
          </div>
          <br />
          <Button
            variant="contained"
            onClick={handleLogin}
            style={{ width: "10rem" }}
          >
            Login
          </Button>
        </form>
        <div className="buttons">
          <Typography variant="p">Create Account ?</Typography>
          <Link to="/signup">
            <Button sx={buttonStyle}>Sign Up</Button>
          </Link>
        </div>
        <div className="buttons">
          <Typography variant="p">Forgot Password ?</Typography>
          <Link to="/resetpassword">
            <Button sx={buttonStyle}>Reset</Button>
          </Link>
        </div>
      </Card>
    </div>
  );
};

export default Login;
