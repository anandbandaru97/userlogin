import React, { useEffect, useState } from "react";
import {
  Card,
  Typography,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Container,
} from "@mui/material";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import UpdateUser from "./UpdateUser";

const User = () => {
  const { username } = useParams();
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const handleClick = () => {
    navigate("/");
  };

  const apiUrl = `http://localhost:7000/api/user/users`;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const token = localStorage.getItem("token");
        const res = await axios.post(apiUrl, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        const { userData } = res.data;
        if (userData) {
          setUsers(userData);
        } else {
          setError("Failed to fetch data");
        }
      } catch (error) {
        setError("Axios Error: " + error.message);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [apiUrl, username]);

  const userStyle = {
    padding: "1rem",
    display: "flex",
    justifyContent: "space-between",
  };

  const tableItems = {
    height: "75vh",
    padding: "1rem",
    overflowY: "auto",
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <>
      <Card sx={userStyle}>
        <Typography variant="h4">Welcome, {username}!</Typography>
        <Button variant="contained" color="error" onClick={handleClick}>
          Logout
        </Button>
      </Card>

      <Container sx={tableItems}>
        {users.map((userData) => (
          <TableContainer key={userData.id}>
            <div className="buttons">
              <Typography variant="h5">
                {userData.username}'s Details
              </Typography>
              <UpdateUser style={{ float: "left" }} user={userData} />
            </div>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Email</TableCell>
                  <TableCell>Age</TableCell>
                  <TableCell>DOB</TableCell>
                  <TableCell>Qualification</TableCell>
                  <TableCell>Office</TableCell>
                  <TableCell>Role</TableCell>
                  <TableCell>Mobile</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>{userData.email}</TableCell>
                  <TableCell>{userData.age}</TableCell>
                  <TableCell>{userData.dob}</TableCell>
                  <TableCell>{userData.qualification}</TableCell>
                  <TableCell>{userData.office}</TableCell>
                  <TableCell>{userData.role}</TableCell>
                  <TableCell>{userData.mobile}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        ))}
      </Container>
    </>
  );
};

export default User;
