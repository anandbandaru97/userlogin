import React, { useState } from "react";
import {
  Alert,
  TextField,
  Typography,
  Button,
  IconButton,
} from "@mui/material";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const apiUrl = "http://localhost:7000/api/user/update-password";

function ResetPassword() {
  const [email, setEmail] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [success, setSuccess] = useState(null);
  const navigate = useNavigate();

  const handleResetPassword = async () => {
    try {
      const response = await axios.post(apiUrl, {
        email: email,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
      });

      if (response.status === 200) {
        // Password reset successful
        setSuccess("Password updated successfully");
        setEmail("");
        setNewPassword("");
        setConfirmPassword("");
        setTimeout(() => {
          navigate("/");
        }, 1000);
      }
    } catch (error) {
      if (error.response) {
        const errorMessage = error.response.data.success;
        setSuccess(`Password update failed: ${errorMessage}`);
      } else {
        setSuccess("Password update failed");
      }
    }
  };

  const handleClickShowPassword = () => {
    setShowPassword((show) => !show);
  };

  return (
    <div className="reset">
      <Typography variant="h5">Password Reset</Typography>
      {success && (
        <Alert variant="filled" severity="success">
          {success}
        </Alert>
      )}
      <br />
      <TextField
        label="Email"
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        style={{width: "16.5rem"}}
      />
      <br />
      <br />
      <TextField
        label="New Password"
        type={showPassword ? "text" : "password"}
        value={newPassword}
        onChange={(e) => setNewPassword(e.target.value)}
        InputProps={{
          endAdornment: (
            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          ),
        }}
      />
      <br />
      <br />
      <TextField
        label="Confirm Password"
        type={showPassword ? "text" : "password"}
        value={confirmPassword}
        onChange={(e) => setConfirmPassword(e.target.value)}
        InputProps={{
          endAdornment: (
            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          ),
        }}
      />
      <br />
      <br />
      <Button variant="contained" onClick={handleResetPassword}>
        Reset Password
      </Button>
      <br />
      <br />
    </div>
  );
}

export default ResetPassword;
