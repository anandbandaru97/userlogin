// cookieUtils.js
import Cookies from "js-cookie";

export function setAuthToken(token) {
  Cookies.set("authToken", token, { expires: 1, path: "/" }); // Example: Set the cookie to expire in 7 days
}

export function getAuthToken() {
  return Cookies.get("authToken");
}

export function removeAuthToken() {
  Cookies.remove("authToken", { path: "/" });
}
